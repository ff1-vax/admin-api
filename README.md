# ff1Vax
## Fictional Vaccine System 
### Created as part of the ff1 High Availability System Training

## This repo (admin-api)

Admin api is responsible for:

- [ ] Enter the lots of vaccine received


### Branches

- [healthcheck](https://gitlab.com/ff1-vax/admin-api/-/tree/healthcheck) Implements the basic node express api code
- [docker](https://gitlab.com/ff1-vax/admin-api/-/tree/docker) Implements dockerfile for prod and dev plus docker-compose file to run the local dev environment
- [persistence](https://gitlab.com/ff1-vax/admin-api/-/tree/persistence) Add sequelize and spinup a pg database. Seed a few records to test the environment


### How to run?

- run docker-compose to spin up the dev environment:
``` docker-compose up -d --build ```
- Check in http://localhost:4000/health (helthcheck)
- Check in http://localhost:4000/ (list all the vaccines from the database)

### docker

Prod image:
- image build 
``` docker build -f dockerfile.prod . -t ff1vax-admin-api ```

- run prod docker image into the local machine
``` docker run -d -p 4000:4000 ff1vax-admin-api ```

Dev image:
 - image build 
``` docker build -f dockerfile.dev . -t ff1vax-admin-api:dev ```

