import Sequelize from 'sequelize'
import express from 'express'

const db = new Sequelize(
    `postgres://${process.env.POSTGRES_HOST}/vax`,
    //`postgres://postgres:mysecretpassword@localhost:5432/vax`,
    { logging: false }
);

db.authenticate().then(() => {
    console.log('Database connected...');
}).catch(err => {
    console.log('Error: ' + err);
})

const vaxDb = db.define("vax", {    
    lot: Sequelize.STRING,
    quantity: Sequelize.BIGINT
});

const app = express()

app.get('/health', (_, res) => {
  res.json({'healthcheck':'alive'})
})
app.get('/', async (_,res) => {
  res.json(await vaxDb.findAll())
})

db.sync({ force: true }).then(() => {
  //Some seed data:
  vaxDb.bulkCreate([
    { lot: '123', quantity: 100000},
    { lot: '456', quantity: 500000},
    { lot: '3af', quantity: 150000},
    { lot: 'p32', quantity: 7860000},    
  ])
  app.listen(4000, () => console.log('server running at port 4000'))
}).catch(err => console.log("Error: " + err))

